<?php

namespace application\helpers;

use application\models\User;

class UniqueRule extends \Rakit\Validation\Rule
{
    protected $message = ":attribute :value уже есть в системе";

    protected $fillableParams = ['table', 'column', 'except'];

    protected $pdo;

    public function __construct($pdo)
    {
        $this->pdo = new $pdo;
    }

    /**
     * Валидатор на уникальность email
     * @param $value
     * @return bool
     * @throws \Rakit\Validation\MissingRequiredParameterException
     */
    public function check($value): bool
    {
        $this->requireParameters(['table', 'column']);
        $column = $this->parameter('column');
        $table = $this->parameter('table');
        $except = $this->parameter('except');

        if ($except and $except == $value) {
            return true;
        }
        $data = $this->pdo::where('email', $value)->get()->count();
        return $data === 0;
    }
}
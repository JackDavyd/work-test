<div class="row">
    <div class="col-12">
        Создать страницу с авторизацией пользователя: логин и пароль и реализовать в ней:<br/>
        возможность регистрации пользователя (email, логин, пароль, ФИО),<br/>
        при входе в "личный кабинет" возможность сменить пароль и ФИО.<br/>
        использовать "чистый" PHP 5.6 и выше (без фреймворков) и MySQL 5.5 и выше, дизайн не важен, верстка тоже
        простая. Наворотов не нужно, хотим посмотреть просто Ваш код.
        <br/>
        SQL<br/>

        Есть 2 таблицы<br/>

        таблица пользователей:<br/>
        users<br/>
        ----------<br/>
        `id` int(11)<br/>
        `email` varchar(55)<br/>
        `login` varchar(55)<br/>

        и таблица заказов<br/>
        orders<br/>
        --------<br/>
        `id` int(11)<br/>
        `user_id` int(11)<br/>
        `price` int(11)<br/>

        Необходимо :<br/>
        составить запрос, который выведет список email'лов встречающихся более чем у одного пользователя<br/>
        вывести список логинов пользователей, которые не сделали ни одного заказа<br/>
        вывести список логинов пользователей которые сделали более двух заказов<br/>
    </div>
</div>
<div class="row"> <!-- You can also position the row if need be. -->
    <div class="col-md-12 col-lg-12"><!-- set width of column I wanted mine to stretch most of the screen-->
        <hr style="min-width:85%; background-color:#a1a1a1 !important; height:1px;"/>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <p>Пояснения</p>
        php 7.0+<br/>
        Использовано:<br/>
        Роутер <a href="https://github.com/zaphpa/zaphpa">https://github.com/zaphpa/zaphpa</a><br/>
        ORM <a href="https://github.com/illuminate/database">https://github.com/illuminate/database</a><br/>
        Валидаторы <a href="https://github.com/rakit/validation">https://github.com/rakit/validation</a><br/>
        Флаш сообщения <a
                href="https://github.com/tamtamchik/simple-flash">https://github.com/tamtamchik/simple-flash</a><br/>
        Велосипедов изобретать не стал. Да и писать все это руками 2 дней маловато будет.<br/>
        SQL<br/>
        Запросы из раздела "Необходимо:" - нативный sql через PDO, хотя через ORM было бы проще. Но подумал запросы
        посмотреть хотите.<br/>
        Таблица users одна. На поле email, индекс unique не поставлен, будет мешать выполнению<br/>
        "составить запрос, который выведет список email'лов встречающихся более чем у одного пользователя"<br/>
        Но при регистрации пользователя, на уровне кода, уникальность проверяется. Зарегистрировать
        пользователей<br/>
        с одним адресом, через интерфейс не удастся.<br/>
        Поля ФИО нет. Его функции выполняет поле login, дополнительное поле теряет смысл, избыточность данных.
    </div>
</div>
<div class="row" style="margin-bottom: 100px"></div>
</div>
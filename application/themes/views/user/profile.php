<h5>Личный кабинет</h5>
<form action="/profile" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">Имя</label>
        <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
               value="<?= !empty($user->name) ? $user->name : '' ?>">
        <small id="emailHelp" class="form-text text-muted">От 3 до 100 символов</small>
    </div>
    <p>Изменение пароля</p>
    <div class="form-group">
        <label for="exampleInputPassword1">Старый пароль</label>
        <input type="password" name="old-pass" class="form-control" id="exampleInputPassword1">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Новый пароль</label>
        <input type="password" name="new-pass" class="form-control" id="exampleInputPassword1">
        <small id="emailHelp" class="form-text text-muted">От 6 до 16 символов</small>
    </div>
    <button type="submit" class="btn btn-primary">Изменить</button>
</form>

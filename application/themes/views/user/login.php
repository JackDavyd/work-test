<form method="post" action="/login">
    <div class="form-group">
        <label for="exampleInputEmail1">Email</label>
        <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
               value="<?= !empty($data['email']) ? $data['email'] : '' ?>">
        <small id="emailHelp" class="form-text text-muted">Не более 100 символов</small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Пароль</label>
        <input type="password" name="password" class="form-control" id="exampleInputPassword1"
               value="<?= !empty($data['password']) ? $data['password'] : '' ?>">
        <small id="emailHelp" class="form-text text-muted">От 6 до 16 символов</small>
    </div>
    <button type="submit" class="btn btn-primary">Логин</button>
</form>

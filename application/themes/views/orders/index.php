<div class="row">
    <div class="col-6">
        <p>Все заказы</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Пользователь</th>
                <th scope="col">цена</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($orders as $order) : ?>
                <tr>
                    <th scope="row"><?= $order->id ?></th>
                    <td><?= $order->user->name ?></td>
                    <td><?= $order->price ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-6">
        <p>список email'лов встречающихся более чем у одного пользователя</p>
        <p>(всегда пустая т.к. поле email в таблице users уникально)</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">email</th>
                <th scope="col">количество раз</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users3 as $user) : ?>
                <tr>
                    <td><?= $user->email ?></td>
                    <td><?= $user->mc ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="row">
    <div class="col-6">
        <p>вывести список логинов пользователей, которые не сделали ни одного заказа</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Пользователь</th>
                <th scope="col">Количество заказов</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users as $user) : ?>
                <tr>
                    <th scope="row"><?= $user->id ?></th>
                    <td><?= $user->name ?></td>
                    <td><?= $user->co ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-6">
        <p>вывести список логинов пользователей которые сделали более двух заказов</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Пользователь</th>
                <th scope="col">Количество заказов</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($users2 as $user) : ?>
                <tr>
                    <th scope="row"><?= $user->id ?></th>
                    <td><?= $user->name ?></td>
                    <td><?= $user->co ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
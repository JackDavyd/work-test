<!doctype html>
<html lang="ru">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/glDatePicker.default.css">
    <link rel="stylesheet" href="/assets/css/styles.css">
    <?php

    use \Tamtamchik\SimpleFlash\Flash;

    ?>
    <title><?= $title ?></title>
</head>
<body>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="/">Test</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav">

                <a class="nav-item nav-link<?= $action == '' || $action == '/home' ? ' active' : ''; ?>" href="/">Главная</a>
                <a class="nav-item nav-link<?= $action == 'orders' ? ' active' : ''; ?>" href="/orders">Заказы</a>
                <?php if (!application\App::user()) : ?>
                    <a class="nav-item nav-link<?= $action == 'login' ? ' active' : ''; ?>" href="/login">Логин</a>
                    <a class="nav-item nav-link<?= $action == 'register' ? ' active' : ''; ?>" href="/register">Регистрация</a>
                <?php else: ?>
                    <div class="btn-group">
                        <button type="button" class="btn btn-primary dropdown-toggle pull-right" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Здравствуйте <?= !empty(application\App::user()->name) ? application\App::user()->name : application\App::user()->email ?>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="/profile">Личный кабинет</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/logout">Выход</a>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </nav>
    <?php
    if (Flash::hasMessages('error')) {
        echo Flash::display('error');
    }
    if (Flash::hasMessages('success')) {
        echo Flash::display('success');
    }
    ?>
    <?= $content ?>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
<script src="/assets/js/glDatePicker.js"></script>
<script src="/assets/js/script.js"></script>
</body>
</html>

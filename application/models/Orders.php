<?php


namespace application\models;


use application\App;

class Orders extends \core\Model
{
    protected $table = 'orders';
    public $timestamps = false;

    /**
     * Связь с таблицей users
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('application\models\User', 'user_id', 'id');
    }

    /**
     * Запросы из раздела Необходимо: в ТЗ
     * @return array
     */
    public static function getSqlQuery()
    {
        $dbh = App::pdo();
        $sql = 'SELECT u.*, count(o.id) co
                FROM users u 
                LEFT JOIN orders o ON u.id=o.user_id
                GROUP BY u.id
                HAVING co = 0';
        $sql2 = 'SELECT u.*, count(o.id) co
                FROM users u 
                LEFT JOIN orders o ON u.id=o.user_id
                GROUP BY u.id
                HAVING co > 2';
        $sql3 = 'SELECT u.email, count(u.email) mc
                FROM users u                 
                GROUP BY u.email
                HAVING mc > 1';
        $stmt = $dbh->query($sql);
        $stmt2 = $dbh->query($sql2);
        $stmt3 = $dbh->query($sql3);
        return [
            'users' => $stmt->fetchAll(),
            'users2' => $stmt2->fetchAll(),
            'users3' => $stmt3->fetchAll(),
        ];
    }
}
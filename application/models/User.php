<?php


namespace application\models;

use application\helpers\UniqueRule;

class User extends \core\Model
{
    protected $table = 'users';
    public $timestamps = false;
    public $errors = [];
    protected $fillable = ['name', 'email', 'password'];
    protected $guarded = ['password'];

    /**
     * Связь с таблицей orders
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user()
    {
        return $this->hasMany('application\models\Orders', 'user_id', 'id');
    }

    /**
     * Регистрация пользователя
     * @param array $data
     * @return bool
     * @throws \Rakit\Validation\RuleQuashException
     */
    public function register($data)
    {
        $validator = new \Rakit\Validation\Validator;
        $validator->addValidator('unique', new UniqueRule(User::class));
        $validator->setMessages([
            'required' => 'Поле :attribute обязательно',
            'min' => 'В поле :attribute не менее :min символов',
            'max' => 'В поле :attribute не более :max символов',
            'confirm_password' => 'Поле повторите пароль не совпадает с полем пароль',
            'unique' => 'Email :value уже есть в системе.',
        ]);

        $validation = $validator->validate($data, [
            'name' => 'min:3|max:100',
            'email' => 'required|email|max:100|unique:users,email,exception@mail.com',
            'password' => 'required|min:6|max:16',
            'confirm_password' => 'required|same:password',
        ]);

        if ($validation->fails()) {
            $this->errors = $validation->errors()->all();
            return false;
        }

        $data['password'] = password_hash($data['password'], PASSWORD_BCRYPT);
        $this->fill($data);
        return $this->save();
    }


    /**
     * Авторизация пользователя
     * @param array $data
     * @return bool
     */
    public function login($data)
    {
        $validator = new \Rakit\Validation\Validator;
        $validator->setMessages([
            'required' => 'Поле :attribute обязательно',
            'email' => 'Не валидный :attribute :value',
            'min' => 'В поле :attribute не менее :min символов',
            'max' => 'В поле :attribute не более :max символов',
        ]);

        $validation = $validator->validate($data, [
            'email' => 'required|email|max:100',
            'password' => 'required|min:6|max:16',
        ]);

        if ($validation->fails()) {
            $this->errors = $validation->errors()->all();
            return false;
        }

        $user = User::where('email', $data['email'])->first();
        if (empty($user->password)) {
            $this->errors[] = 'Пользователь не найден';
            return false;
        }
        if (!password_verify($data['password'], $user->password)) {
            $this->errors[] = 'Пароль не верен';
            return false;
        }
        $_SESSION['user_id'] = $user->id;
        return true;
    }
}
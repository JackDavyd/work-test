<?php


namespace application;


use application\models\User;

class App
{
    public static $user;
    public static $config;

    public function run()
    {
        self::$config = require __DIR__ . '/config/db.php';
        //Инициализация роутера
        $router = new \Zaphpa\Router();
        $router->attach('\Zaphpa\Middleware\AutoDocumentator', '/apidocs'); //auto-docs middleware
        $router->attach('\Zaphpa\Middleware\MethodOverride');
        $router->attach('\Zaphpa\Middleware\CORS');

        $router->addRoute([
            'path' => "/",
            'get' => ['application\controllers\HomeController', 'index'],
        ]);

        $router->addRoute(array(
            'path' => "/login",
            'get' => ['application\controllers\UserController', 'login'],
            'post' => array('application\controllers\UserController', 'login'),

        ));

        $router->addRoute(array(
            'path' => "/register",
            'get' => ['application\controllers\UserController', 'register'],
            'post' => array('application\controllers\UserController', 'register'),
        ));

        $router->addRoute(array(
            'path' => "/logout",
            'get' => ['application\controllers\UserController', 'logout'],
        ));

        $router->addRoute(array(
            'path' => "/profile",
            'get' => ['application\controllers\UserController', 'profile'],
            'post' => array('application\controllers\UserController', 'profile'),
        ));

        $router->addRoute(array(
            'path' => "/orders",
            'get' => ['application\controllers\OrdersController', 'index'],
        ));

        try {
            $router->route();
        } catch (\Zaphpa\Exceptions\InvalidPathException $ex) {
            header('Status: 403 Forbidden');
            header('HTTP/1.1 403 Forbidden');
            exit();
        }
    }

    /**
     * Соединение с БД через PDO
     * @return \PDO
     */
    public static function pdo()
    {
        $config = App::$config;
        try {
            $dbh = new \PDO("{$config['driver']}:host={$config['host']};dbname={$config['database']}",
                $config['username'], $config['password'],
                [
                    \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_OBJ,
                    \PDO::ATTR_EMULATE_PREPARES => false,
                ]
            );
        } catch (\PDOException $e) {
            echo 'Подключение не удалось: ' . $e->getMessage();
            die();
        }
        return $dbh;
    }

    /**
     * Обьект авторизованного юзера
     * @return false
     */
    public static function user()
    {
        if (!self::$user) {
            if (!empty($_SESSION['user_id'])) {
                self::$user = User::find($_SESSION['user_id']);
            } else {
                self::$user = false;
            }
        }
        return self::$user;
    }

}

<?php


namespace application\controllers;
use core\Controller;
use application\models\Home as HomeModel;

class HomeController extends Controller
{
    /**
     * Главная
     * @param $request
     * @param $res
     * @throws \ReflectionException
     */
    public function index($request, $res)
    {
        return $this->render('index',[
            'action'=>'',
            'title' => 'Главная',
        ]);
    }
}
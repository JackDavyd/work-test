<?php


namespace application\controllers;

use application\App;
use application\helpers\UniqueRule;
use application\models\User;

class UserController extends \core\Controller
{
    /**
     * Страница логина
     * @param $request
     * @param $res
     * @throws \ReflectionException
     */
    public function login($request, $res)
    {
        if (App::user()) {
            flash()->error("Пользователь {$request->user->name} уже авторизован.");
            return $this->redirect('/');
        }
        if ($request->method == 'post') {
            $model = new User();
            if ($model->login($request->data)) {
                flash()->success("Пользователь авторизован. Здравствуйте " . App::user()->name);
                return $this->redirect('/');
            } else {
                flash()->error($model->errors);
            }
        }
        return $this->render('login', [
            'action' => 'login',
            'data' => $request->data,
            'title' => 'Логин',
        ]);


    }

    /**
     * Страница регистрации
     * @param $request
     * @param $res
     * @throws \ReflectionException
     */
    public function register($request, $res)
    {
        if (App::user()) {
            flash()->error("Пользователь {$request->user->name} уже авторизован.");
            return $this->redirect('/');
        }
        if ($request->method == 'post') {
            $model = new User();
            if (!$model->register($request->data) && $model->errors) {
                flash()->error($model->errors);
                return $this->render('register', [
                    'action' => 'register',
                    'data' => $request->data,
                    'title' => 'Регистрация',
                ]);
            } else {
                flash()->success('Пльзователь зарегистирован. Войдите по вашим реквизитам.');
                return $this->redirect('/');
            }
        } else {
            $this->render('register', [
                'action' => 'register',
                'title' => 'Регистрация',
            ]);
        }
    }

    /**
     * Страница выхода
     * @param $request
     * @param $res
     * @throws \ReflectionException
     */
    public function logout()
    {
        if (!empty($_SESSION['user_id'])) {
            unset($_SESSION['user_id']);
        }
        $this->redirect('/');
    }

    /**
     * Страница профиля
     * изменение данных в модель выносить не стал, для наглядности.
     * @param $request
     * @param $res
     * @throws \ReflectionException
     */
    public function profile($request)
    {
        $userId = App::user()->id;
        $user = User::find($userId);
        if ($request->method == 'post') {
            $data = array_filter($request->data);
            $validator = new \Rakit\Validation\Validator;
            $validator->addValidator('unique', new UniqueRule(User::class));
            $validator->setMessages([
                'min' => 'В поле :attribute не менее :min символов',
                'max' => 'В поле :attribute не более :max символов',
            ]);

            $validation = $validator->validate($data, [
                'name' => 'min:3|max:100',
                'new_pass' => 'min:6|max:16',
                'old_pass' => 'min:6|max:16',
            ]);

            if ($validation->fails()) {
                $this->errors = $validation->errors()->all();
                return $this->render('profile', [
                    'action' => 'profile',
                    'user' => $user,
                    'title' => 'Профиль',
                ]);
            }

            $user->name = !empty($data['name']) ? $data['name'] : $user->name;
            if (!empty($data['new-pass'])) {
                if (password_verify($data['old-pass'], $user->password)) {
                    $user->password = password_hash($data['new-pass'], PASSWORD_BCRYPT);
                } else {
                    flash()->error('Старый пароль не верен. Пароль не изменен.');
                }
            }
            if ($user->save()) {
                flash()->success('Изменения сохранены');
                $user = User::find($userId);
                return $this->render('profile', [
                    'action' => 'profile',
                    'user' => $user,
                    'title' => 'Профиль',
                ]);
            }
        }
        return $this->render('profile', [
            'action' => 'profile',
            'user' => $user,
            'title' => 'Профиль',
        ]);
    }
}
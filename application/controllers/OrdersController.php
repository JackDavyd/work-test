<?php


namespace application\controllers;

use application\models\Orders;

class OrdersController extends \core\Controller
{
    /**
     * Страница заказов
     * @param $reques
     * @throws \ReflectionException
     */
    public function index($reques)
    {
        $data = [
            'action' => 'orders',
            'orders' => Orders::with(['user'])->get(),
            'title' => 'Заказы',
        ];
        $native = Orders::getSqlQuery();
        if ($native) {
            $data = array_merge($data, $native);
        }
        return $this->render('index', $data);
    }
}
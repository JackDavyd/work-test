-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 05, 2020 at 01:27 PM
-- Server version: 8.0.21
-- PHP Version: 7.3.19-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `work`
--

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `price` double(11,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `price`) VALUES
(1, 3, 1000.56),
(4, 3, 5000.89),
(6, 3, 59999.55),
(7, 3, 78787.59),
(8, 1, 7465.44),
(9, 1, 4545.78),
(10, 1, 4545.21),
(11, 4, 4545.44),
(12, 4, 45.77),
(13, 4, 54545.00);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` char(100) NOT NULL,
  `password` char(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`) VALUES
(1, 'erererer12345', 'fsssss@ytttt1.ru', '$2y$10$8lTphPX50CloPj45ndHQqOzUjWDna2X1LUVAIVb3Tsv2J4jNuVl7i'),
(2, 'erererer2', 'fsssss@ytttt2.ru', '$2y$10$iIxMU5MkTsJ5tJb/gwLzce8pDdK0OjkEHzOwE2w2GxprDzHGb6S42'),
(3, 'erererer52', 'fsssss@ytttt.ru', '$2y$10$ZsPFJ5THOary/oQC5wYrNeBwmbmUgCp1WMEiBJiVp8Fq6c1S.F0gy'),
(4, 'erererer33', 'fsssss@ytttt33.ru', '$2y$10$yEfnbDAfR6xsCnX51FuJ4etmOiXonk4rga/Z7OQI0EbOtRlG8ecBK'),
(5, 'rrrr', 'fsssss@ytttt.ru', 'rrrrrrr'),
(6, 'aaaa', 'fsssss@ytttt.ru', 'gdgdfgdfg'),
(7, 'erererer111', 'fsssss@ytttt11111.ru', '$2y$10$chgUZDA3Sm38IPaqZ8LCs.F6VctihmUEco7oYsDYIqxGFeq2rkgYS');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

namespace core;

use ReflectionException;
use ReflectionClass;
use \Tamtamchik\SimpleFlash\Flash;

class Controller
{
    public $layout = 'layout';
    public $themesPath = __DIR__ . '/../application/themes';

    /**
     * Редирект
     * @param string $url Урл куда редиректить
     */
    public function redirect($url)
    {
        header("Location: {$url}");
        die();
    }


    /**
     * Подключение верстки
     * @param string $view имя файла вертстки без .php
     * @param array $data Массив данных для подстановки в верстку
     * @throws ReflectionException
     */
    public function render($view, $data = [])
    {
        $class = (new ReflectionClass(get_class($this)))->getShortName();
        $viewDir = strtolower(preg_replace('/Controller/', '', $class));
        $content = null;
        extract($data);
        ob_start();
        require($this->themesPath . '/views/' . $viewDir . '/' . $view . '.php');
        $content = ob_get_clean();
        require((string)$this->themesPath . '/layouts/' . (string)$this->layout . '.php');
    }
}
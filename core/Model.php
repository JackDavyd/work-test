<?php

namespace core;

use application\App;
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

class Model extends \Illuminate\Database\Eloquent\Model
{
    /**
     * Инициализация ORM
     * Model constructor.
     */
    function __construct()
    {
        $capsule = new Capsule;
        $capsule->addConnection(App::$config);
        $capsule->setEventDispatcher(new Dispatcher(new Container));
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}
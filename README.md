
Установка
-------------------
Предполагается что git и composer установлены глобально

В каталоге DOCUMENT_ROOT выполнить
````
git clone https://JackDavyd@bitbucket.org/JackDavyd/work-test.git .
composer install
````
Бекап БД в каталоге /data, импортировать в БД
Подключить базу данных в файле /config/db.php.
Должно заработать

В архиве /work.zip собранное приложение. Распаковать в корень, импортировать БД и подключить в /config/db.php.
